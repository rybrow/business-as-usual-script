#!/bin/bash

sleep_random() {
    min_sleep=$1
    max_sleep=$2
    interval=$(shuf -i$min_sleep-$max_sleep -n1)
    sleep $interval
}

type() {
  command_name=$1
  while [ ${#command_name} -gt 0 ]; do
    printf '%s' "${command_name%${command_name#?}}"
    command_name=${command_name#?}
    sleep 0.1
  done
  printf '\n'
}

print_command() {
  eval_command=$1
  type $eval_command
  eval "$eval_command"
}

print_employee_command() {

  commands=(
    "ls -lR ."
    "free -h"
    "du -h -d1"
    "du"
    "df"
    "ps aux"
    "du -h -d1 /"
    "netstat -a"
    "apt list --installed"
    "top"
    "free -m"
    "iostat -x"
    "tcpdump"
    "df -a"
    "ls -R /etc"
  )

  employee_id=$1
  machine_id=$2
  command_type=$(shuf -i0-${#commands[@]} -n1)
  echo -n "${employee_id}@AKLVM${machine_id}:/home/user/${employee_id}\$ "
  print_command "${commands[${command_type}]}"
}

print_employee_details() {
  while true; do
    employee_id=$(shuf -i100000-999999 -n1)
    machine_id=$(shuf -i10-50 -n1)
    type "Logging in as user ${employee_id}"
    sleep 3
    figlet "The Company" -f slant
    employee_command_count=$(shuf -i5-20 -n1)
    type "Employee ${employee_id}: Logged in to AKLVM${machine_id}"
    for i in $(seq 1 $employee_command_count);
    do
      print_employee_command $employee_id $machine_id
      sleep_random 1 3
    done
    sleep_random 1 5
    type "Logging out user ${employee_id}"
    sleep 3
    reset
  done
}

randomly_resize_screen() {
  # Set Initial Screen Size
  width=$(shuf -i80-150 -n1)
  height=$(shuf -i20-40 -n1)

  # echo "Initial Width: $width"
  # echo "Initial Height: $height"

  printf "\033[8;${height};${width}t"

  while true; do
    target_width=$(shuf -i80-150 -n1)
    target_height=$(shuf -i20-40 -n1)
    while [ "$target_width" != "$width" ] && [ "$target_height" != "$height" ]; do
      if [ "$target_width" -gt "$width" ]; then
        width=$((width + 1))
      fi
      if [ "$target_width" -lt "$width" ]; then
        width=$((width - 1))
      fi
      if [ "$target_height" -gt "$height" ]; then
        height=$((height + 1))
      fi
      if [ "$target_height" -lt "$height" ]; then
        height=$((height - 1))
      fi
      # echo "Width: $width, Target: $target_width"
      # echo "Height: $height, Target: $target_height"
      printf "\033[8;${height};${width}t"
      sleep 0.0333333 # Mimic 30fps
    done
    sleep_random 3 5
  done
}

# Trap SIGINT and SIGTERM to prevent the script from being killed by Ctrl+C or Ctrl+X
trap '' SIGINT SIGTERM

# Start the functions in the background and store PIDs
print_employee_details &
print_employee_details_pid=$!

randomly_resize_screen &
randomly_resize_screen_pid=$!

# User input loop
while true; do
  read input
done

# Restore default signal handling and kill the background process before exiting
trap - SIGINT SIGTERM
kill $print_employee_details_pid
kill $randomly_resize_screen_pid
echo "Background process killed. Exiting script."
