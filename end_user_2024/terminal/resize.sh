# Set Initial Screen Size
width=$(shuf -i80-150 -n1)
height=$(shuf -i20-40 -n1)

# echo "Initial Width: $width"
# echo "Initial Height: $height"

printf "\033[8;${height};${width}t"

while true; do
  target_width=$(shuf -i80-150 -n1)
  target_height=$(shuf -i20-40 -n1)
  while [ "$target_width" != "$width" ] && [ "$target_height" != "$height" ]; do
    if [ "$target_width" -gt "$width" ]; then
      width=$((width + 1))
    fi
    if [ "$target_width" -lt "$width" ]; then
      width=$((width - 1))
    fi
    if [ "$target_height" -gt "$height" ]; then
      height=$((height + 1))
    fi
    if [ "$target_height" -lt "$height" ]; then
      height=$((height - 1))
    fi
    # echo "Width: $width, Target: $target_width"
    # echo "Height: $height, Target: $target_height"
    printf "\033[8;${height};${width}t"
    sleep 0.05
  done
done