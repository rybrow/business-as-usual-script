#!/bin/bash

sleep_random() {
    min_sleep=$1
    max_sleep=$2
    interval=$(shuf -i$min_sleep-$max_sleep -n1)
    sleep $interval
}


while true
do
    scanimage --format jpeg --resolution 300dpi > /var/tmp/image.jpeg
    sleep_random 10 30
done